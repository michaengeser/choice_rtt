Authors: Micha Engeser (michaengeser@gmail.com)
Collaborators: Lenny and Philipp
Date of creation: 12.02.24
For information about the methods and the analysis details see: Gitlab: https://gitlab.pavlovia.org/demos/choicertt
Also see preregistration see: https://osf.io/swkjp/
