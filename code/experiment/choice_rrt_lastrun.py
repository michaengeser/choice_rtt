﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy3 Experiment Builder (v2023.2.3),
    on Februar 13, 2024, at 16:32
If you publish work using this script the most relevant publication is:

    Peirce J, Gray JR, Simpson S, MacAskill M, Höchenberger R, Sogo H, Kastman E, Lindeløv JK. (2019) 
        PsychoPy2: Experiments in behavior made easy Behav Res 51: 195. 
        https://doi.org/10.3758/s13428-018-01193-y

"""

import psychopy
psychopy.useVersion('2023.2.3')


# --- Import packages ---
from psychopy import locale_setup
from psychopy import prefs
from psychopy import plugins
plugins.activatePlugins()
prefs.hardware['audioLib'] = 'ptb'
prefs.hardware['audioLatencyMode'] = '3'
from psychopy import sound, gui, visual, core, data, event, logging, clock, colors, layout
from psychopy.tools import environmenttools
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER, priority)

import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle, choice as randchoice
import os  # handy system and path functions
import sys  # to get file system encoding

import psychopy.iohub as io
from psychopy.hardware import keyboard

# --- Setup global variables (available in all functions) ---
# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__))
# Store info about the experiment session
psychopyVersion = '2023.2.3'
expName = 'choice_rrt'  # from the Builder filename that created this script
expInfo = {
    'participant': '',
    'session': '',
    'gender': '',
    'age': '',
    'left-handed': False,
    'do you like that session?': ['yes', 'no', 'maybe'],
    'output-path': '../../sourcedata',
    'date': data.getDateStr(),  # add a simple timestamp
    'expName': expName,
    'psychopyVersion': psychopyVersion,
}


def showExpInfoDlg(expInfo):
    """
    Show participant info dialog.
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    
    Returns
    ==========
    dict
        Information about this experiment.
    """
    # temporarily remove keys which the dialog doesn't need to show
    poppedKeys = {
        'date': expInfo.pop('date', data.getDateStr()),
        'expName': expInfo.pop('expName', expName),
        'psychopyVersion': expInfo.pop('psychopyVersion', psychopyVersion),
    }
    # show participant info dialog
    dlg = gui.DlgFromDict(dictionary=expInfo, sortKeys=False, title=expName)
    if dlg.OK == False:
        core.quit()  # user pressed cancel
    # restore hidden keys
    expInfo.update(poppedKeys)
    # return expInfo
    return expInfo


def setupData(expInfo, dataDir=None):
    """
    Make an ExperimentHandler to handle trials and saving.
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    dataDir : Path, str or None
        Folder to save the data to, leave as None to create a folder in the current directory.    
    Returns
    ==========
    psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    """
    
    # data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
    if dataDir is None:
        dataDir = _thisDir
    filename = u'%s/sub-%s/ses-%s/%s_%s_%s' % (expInfo['output-path'], expInfo['participant'], expInfo['session'], expInfo['participant'], expInfo['session'], expName)
    # make sure filename is relative to dataDir
    if os.path.isabs(filename):
        dataDir = os.path.commonprefix([dataDir, filename])
        filename = os.path.relpath(filename, dataDir)
    
    # an ExperimentHandler isn't essential but helps with data saving
    thisExp = data.ExperimentHandler(
        name=expName, version='',
        extraInfo=expInfo, runtimeInfo=None,
        originPath='C:\\Users\\JLU-SU\\OneDrive - Justus-Liebig-Universität Gießen\\Desktop\\NOWA_school\\choice_rtt\\code\\experiment\\choice_rrt_lastrun.py',
        savePickle=True, saveWideText=False,
        dataFileName=dataDir + os.sep + filename, sortColumns='time'
    )
    thisExp.setPriority('thisRow.t', priority.CRITICAL)
    thisExp.setPriority('expName', priority.LOW)
    # return experiment handler
    return thisExp


def setupLogging(filename):
    """
    Setup a log file and tell it what level to log at.
    
    Parameters
    ==========
    filename : str or pathlib.Path
        Filename to save log file and data files as, doesn't need an extension.
    
    Returns
    ==========
    psychopy.logging.LogFile
        Text stream to receive inputs from the logging system.
    """
    # this outputs to the screen, not a file
    logging.console.setLevel(logging.EXP)
    # save a log file for detail verbose info
    logFile = logging.LogFile(filename+'.log', level=logging.EXP)
    
    return logFile


def setupWindow(expInfo=None, win=None):
    """
    Setup the Window
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    win : psychopy.visual.Window
        Window to setup - leave as None to create a new window.
    
    Returns
    ==========
    psychopy.visual.Window
        Window in which to run this experiment.
    """
    if win is None:
        # if not given a window to setup, make one
        win = visual.Window(
            size=[1280, 720], fullscr=True, screen=0,
            winType='pyglet', allowStencil=False,
            monitor='testMonitor', color=[0,0,0], colorSpace='rgb',
            backgroundImage='', backgroundFit='none',
            blendMode='avg', useFBO=True,
            units='height'
        )
        if expInfo is not None:
            # store frame rate of monitor if we can measure it
            expInfo['frameRate'] = win.getActualFrameRate()
    else:
        # if we have a window, just set the attributes which are safe to set
        win.color = [0,0,0]
        win.colorSpace = 'rgb'
        win.backgroundImage = ''
        win.backgroundFit = 'none'
        win.units = 'height'
    win.mouseVisible = False
    win.hideMessage()
    return win


def setupInputs(expInfo, thisExp, win):
    """
    Setup whatever inputs are available (mouse, keyboard, eyetracker, etc.)
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    win : psychopy.visual.Window
        Window in which to run this experiment.
    Returns
    ==========
    dict
        Dictionary of input devices by name.
    """
    # --- Setup input devices ---
    inputs = {}
    ioConfig = {}
    
    # Setup iohub keyboard
    ioConfig['Keyboard'] = dict(use_keymap='psychopy')
    
    ioSession = '1'
    if 'session' in expInfo:
        ioSession = str(expInfo['session'])
    ioServer = io.launchHubServer(window=win, **ioConfig)
    eyetracker = None
    
    # create a default keyboard (e.g. to check for escape)
    defaultKeyboard = keyboard.Keyboard(backend='iohub')
    # return inputs dict
    return {
        'ioServer': ioServer,
        'defaultKeyboard': defaultKeyboard,
        'eyetracker': eyetracker,
    }

def pauseExperiment(thisExp, inputs=None, win=None, timers=[], playbackComponents=[]):
    """
    Pause this experiment, preventing the flow from advancing to the next routine until resumed.
    
    Parameters
    ==========
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    inputs : dict
        Dictionary of input devices by name.
    win : psychopy.visual.Window
        Window for this experiment.
    timers : list, tuple
        List of timers to reset once pausing is finished.
    playbackComponents : list, tuple
        List of any components with a `pause` method which need to be paused.
    """
    # if we are not paused, do nothing
    if thisExp.status != PAUSED:
        return
    
    # pause any playback components
    for comp in playbackComponents:
        comp.pause()
    # prevent components from auto-drawing
    win.stashAutoDraw()
    # run a while loop while we wait to unpause
    while thisExp.status == PAUSED:
        # make sure we have a keyboard
        if inputs is None:
            inputs = {
                'defaultKeyboard': keyboard.Keyboard(backend='ioHub')
            }
        # check for quit (typically the Esc key)
        if inputs['defaultKeyboard'].getKeys(keyList=['escape']):
            endExperiment(thisExp, win=win, inputs=inputs)
        # flip the screen
        win.flip()
    # if stop was requested while paused, quit
    if thisExp.status == FINISHED:
        endExperiment(thisExp, inputs=inputs, win=win)
    # resume any playback components
    for comp in playbackComponents:
        comp.play()
    # restore auto-drawn components
    win.retrieveAutoDraw()
    # reset any timers
    for timer in timers:
        timer.reset()


def run(expInfo, thisExp, win, inputs, globalClock=None, thisSession=None):
    """
    Run the experiment flow.
    
    Parameters
    ==========
    expInfo : dict
        Information about this experiment, created by the `setupExpInfo` function.
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    psychopy.visual.Window
        Window in which to run this experiment.
    inputs : dict
        Dictionary of input devices by name.
    globalClock : psychopy.core.clock.Clock or None
        Clock to get global time from - supply None to make a new one.
    thisSession : psychopy.session.Session or None
        Handle of the Session object this experiment is being run from, if any.
    """
    # mark experiment as started
    thisExp.status = STARTED
    # make sure variables created by exec are available globally
    exec = environmenttools.setExecEnvironment(globals())
    # get device handles from dict of input devices
    ioServer = inputs['ioServer']
    defaultKeyboard = inputs['defaultKeyboard']
    eyetracker = inputs['eyetracker']
    # make sure we're running in the directory for this experiment
    os.chdir(_thisDir)
    # get filename from ExperimentHandler for convenience
    filename = thisExp.dataFileName
    frameTolerance = 0.001  # how close to onset before 'same' frame
    endExpNow = False  # flag for 'escape' or other condition => quit the exp
    # get frame duration from frame rate in expInfo
    if 'frameRate' in expInfo and expInfo['frameRate'] is not None:
        frameDur = 1.0 / round(expInfo['frameRate'])
    else:
        frameDur = 1.0 / 60.0  # could not measure, so guess
    
    # Start Code - component code to be run after the window creation
    
    # --- Initialize components for Routine "welcome" ---
    Welcome_message = visual.TextStim(win=win, name='Welcome_message',
        text='Welcome to the experiment!\n\nPress any button to continue to the instruction.\n\n',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    press_space = keyboard.Keyboard()
    
    # --- Initialize components for Routine "instruction" ---
    instruction_text = visual.TextStim(win=win, name='instruction_text',
        text='In this task, you will make decisions as to which stimulus you have seen. There are three versions of the task. In the first, you have to decide which shape you have seen, in the second which image and the third which sound you’ve heard. Before each task, you will get set of specific instructions and short practice period. Please press the space bar to continue.\n',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    key_space = keyboard.Keyboard()
    
    # --- Initialize components for Routine "instruction2" ---
    text = visual.TextStim(win=win, name='text',
        text='In this task you will make a decision as to which shape you have seen.\n\nPress C or click cross for a cross Press V or click square for a square Press B or click plus for a plus\n\nFirst, we will have a quick practice.\n\nPush space bar or click / touch one of the buttons to begin.',
        font='Open Sans',
        pos=(0, 0.1), height=0.035, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    instr2_space = keyboard.Keyboard()
    response_square = visual.ImageStim(
        win=win,
        name='response_square', 
        image='C:/Users/JLU-SU/OneDrive - Justus-Liebig-Universität Gießen/Desktop/NOWA_school/choice_rtt/code/stimuli/shapes/response_square.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-2.0)
    response_plus = visual.ImageStim(
        win=win,
        name='response_plus', 
        image='C:/Users/JLU-SU/OneDrive - Justus-Liebig-Universität Gießen/Desktop/NOWA_school/choice_rtt/code/stimuli/shapes/response_plus.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0.25, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-3.0)
    response_cross = visual.ImageStim(
        win=win,
        name='response_cross', 
        image='C:/Users/JLU-SU/OneDrive - Justus-Liebig-Universität Gießen/Desktop/NOWA_school/choice_rtt/code/stimuli/shapes/response_cross.jpg', mask=None, anchor='center',
        ori=0.0, pos=(-0.25, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-4.0)
    
    # --- Initialize components for Routine "trial" ---
    image_cross = visual.ImageStim(
        win=win,
        name='image_cross', 
        image='C:/Users/JLU-SU/OneDrive - Justus-Liebig-Universität Gießen/Desktop/NOWA_school/choice_rtt/code/stimuli/shapes/response_cross.jpg', mask=None, anchor='center',
        ori=0.0, pos=(-0.25, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-1.0)
    image_plus = visual.ImageStim(
        win=win,
        name='image_plus', 
        image='C:/Users/JLU-SU/OneDrive - Justus-Liebig-Universität Gießen/Desktop/NOWA_school/choice_rtt/code/stimuli/shapes/response_plus.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0.25, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-2.0)
    image_square = visual.ImageStim(
        win=win,
        name='image_square', 
        image='C:/Users/JLU-SU/OneDrive - Justus-Liebig-Universität Gießen/Desktop/NOWA_school/choice_rtt/code/stimuli/shapes/response_square.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-3.0)
    left_far = visual.ImageStim(
        win=win,
        name='left_far', 
        image='C:/Users/JLU-SU/OneDrive - Justus-Liebig-Universität Gießen/Desktop/NOWA_school/choice_rtt/code/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(-0.375, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=True, flipVert=False,
        texRes=128.0, interpolate=True, depth=-4.0)
    right_mid = visual.ImageStim(
        win=win,
        name='right_mid', 
        image='C:/Users/JLU-SU/OneDrive - Justus-Liebig-Universität Gießen/Desktop/NOWA_school/choice_rtt/code/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(0.125, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=True, flipVert=False,
        texRes=128.0, interpolate=True, depth=-5.0)
    right_far = visual.ImageStim(
        win=win,
        name='right_far', 
        image='C:/Users/JLU-SU/OneDrive - Justus-Liebig-Universität Gießen/Desktop/NOWA_school/choice_rtt/code/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(.375, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=True, flipVert=False,
        texRes=128.0, interpolate=True, depth=-6.0)
    left_mid = visual.ImageStim(
        win=win,
        name='left_mid', 
        image='C:/Users/JLU-SU/OneDrive - Justus-Liebig-Universität Gießen/Desktop/NOWA_school/choice_rtt/code/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(-0.125, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=True, flipVert=False,
        texRes=128.0, interpolate=True, depth=-7.0)
    target_image = visual.ImageStim(
        win=win,
        name='target_image', 
        image='default.png', mask=None, anchor='center',
        ori=0.0, pos=[0,0], size=(0.2, 0.2),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-8.0)
    keyboard_response = keyboard.Keyboard()
    
    # --- Initialize components for Routine "practice_feedback" ---
    feedback_text = visual.TextStim(win=win, name='feedback_text',
        text='',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    keyboard_response_feedback_ = keyboard.Keyboard()
    
    # --- Initialize components for Routine "instruction3" ---
    end_practice_text = visual.TextStim(win=win, name='end_practice_text',
        text='Well done, you have finished the practice! \n\nNow for the main experiment. Press space to continue!',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    key_space_2 = keyboard.Keyboard()
    
    # --- Initialize components for Routine "trial" ---
    image_cross = visual.ImageStim(
        win=win,
        name='image_cross', 
        image='C:/Users/JLU-SU/OneDrive - Justus-Liebig-Universität Gießen/Desktop/NOWA_school/choice_rtt/code/stimuli/shapes/response_cross.jpg', mask=None, anchor='center',
        ori=0.0, pos=(-0.25, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-1.0)
    image_plus = visual.ImageStim(
        win=win,
        name='image_plus', 
        image='C:/Users/JLU-SU/OneDrive - Justus-Liebig-Universität Gießen/Desktop/NOWA_school/choice_rtt/code/stimuli/shapes/response_plus.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0.25, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-2.0)
    image_square = visual.ImageStim(
        win=win,
        name='image_square', 
        image='C:/Users/JLU-SU/OneDrive - Justus-Liebig-Universität Gießen/Desktop/NOWA_school/choice_rtt/code/stimuli/shapes/response_square.jpg', mask=None, anchor='center',
        ori=0.0, pos=(0, -0.25), size=(0.2, 0.1),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-3.0)
    left_far = visual.ImageStim(
        win=win,
        name='left_far', 
        image='C:/Users/JLU-SU/OneDrive - Justus-Liebig-Universität Gießen/Desktop/NOWA_school/choice_rtt/code/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(-0.375, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=True, flipVert=False,
        texRes=128.0, interpolate=True, depth=-4.0)
    right_mid = visual.ImageStim(
        win=win,
        name='right_mid', 
        image='C:/Users/JLU-SU/OneDrive - Justus-Liebig-Universität Gießen/Desktop/NOWA_school/choice_rtt/code/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(0.125, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=True, flipVert=False,
        texRes=128.0, interpolate=True, depth=-5.0)
    right_far = visual.ImageStim(
        win=win,
        name='right_far', 
        image='C:/Users/JLU-SU/OneDrive - Justus-Liebig-Universität Gießen/Desktop/NOWA_school/choice_rtt/code/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(.375, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=True, flipVert=False,
        texRes=128.0, interpolate=True, depth=-6.0)
    left_mid = visual.ImageStim(
        win=win,
        name='left_mid', 
        image='C:/Users/JLU-SU/OneDrive - Justus-Liebig-Universität Gießen/Desktop/NOWA_school/choice_rtt/code/stimuli/shapes/white_square.png', mask=None, anchor='center',
        ori=0.0, pos=(-0.125, 0), size=(0.22, 0.22),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=True, flipVert=False,
        texRes=128.0, interpolate=True, depth=-7.0)
    target_image = visual.ImageStim(
        win=win,
        name='target_image', 
        image='default.png', mask=None, anchor='center',
        ori=0.0, pos=[0,0], size=(0.2, 0.2),
        color=[1,1,1], colorSpace='rgb', opacity=None,
        flipHoriz=False, flipVert=False,
        texRes=128.0, interpolate=True, depth=-8.0)
    keyboard_response = keyboard.Keyboard()
    
    # --- Initialize components for Routine "end_screen" ---
    end_message = visual.TextStim(win=win, name='end_message',
        text='You have reached the end of the experiment, thank you very much for participating. Please press the space bar to finish.',
        font='Open Sans',
        pos=(0, 0), height=0.05, wrapWidth=None, ori=0.0, 
        color='white', colorSpace='rgb', opacity=None, 
        languageStyle='LTR',
        depth=0.0);
    space_end = keyboard.Keyboard()
    
    # create some handy timers
    if globalClock is None:
        globalClock = core.Clock()  # to track the time since experiment started
    if ioServer is not None:
        ioServer.syncClock(globalClock)
    logging.setDefaultClock(globalClock)
    routineTimer = core.Clock()  # to track time remaining of each (possibly non-slip) routine
    win.flip()  # flip window to reset last flip timer
    # store the exact time the global clock started
    expInfo['expStart'] = data.getDateStr(format='%Y-%m-%d %Hh%M.%S.%f %z', fractionalSecondDigits=6)
    
    # --- Prepare to start Routine "welcome" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('welcome.started', globalClock.getTime())
    press_space.keys = []
    press_space.rt = []
    _press_space_allKeys = []
    # Run 'Begin Routine' code from createDirectories
    # Construct the path with placeholders replaced by actual values
    dir_path = expInfo['output-path'] + "/sub-" + expInfo['participant'] + "/ses-" + expInfo['session']
    
    # Check if the path exists
    if not os.path.exists(dir_path):
        # Create the directory, including all intermediate directories
        os.makedirs(path)
        print(f"Directory '{dir_path}' was created.")
    else:
        print(f"Directory '{dir_path}' already exists.")
    # keep track of which components have finished
    welcomeComponents = [Welcome_message, press_space]
    for thisComponent in welcomeComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "welcome" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *Welcome_message* updates
        
        # if Welcome_message is starting this frame...
        if Welcome_message.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            Welcome_message.frameNStart = frameN  # exact frame index
            Welcome_message.tStart = t  # local t and not account for scr refresh
            Welcome_message.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(Welcome_message, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'Welcome_message.started')
            # update status
            Welcome_message.status = STARTED
            Welcome_message.setAutoDraw(True)
        
        # if Welcome_message is active this frame...
        if Welcome_message.status == STARTED:
            # update params
            pass
        
        # *press_space* updates
        waitOnFlip = False
        
        # if press_space is starting this frame...
        if press_space.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            press_space.frameNStart = frameN  # exact frame index
            press_space.tStart = t  # local t and not account for scr refresh
            press_space.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(press_space, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'press_space.started')
            # update status
            press_space.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(press_space.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(press_space.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if press_space.status == STARTED and not waitOnFlip:
            theseKeys = press_space.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _press_space_allKeys.extend(theseKeys)
            if len(_press_space_allKeys):
                press_space.keys = _press_space_allKeys[-1].name  # just the last key pressed
                press_space.rt = _press_space_allKeys[-1].rt
                press_space.duration = _press_space_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in welcomeComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "welcome" ---
    for thisComponent in welcomeComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('welcome.stopped', globalClock.getTime())
    # check responses
    if press_space.keys in ['', [], None]:  # No response was made
        press_space.keys = None
    thisExp.addData('press_space.keys',press_space.keys)
    if press_space.keys != None:  # we had a response
        thisExp.addData('press_space.rt', press_space.rt)
        thisExp.addData('press_space.duration', press_space.duration)
    thisExp.nextEntry()
    # the Routine "welcome" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # --- Prepare to start Routine "instruction" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('instruction.started', globalClock.getTime())
    key_space.keys = []
    key_space.rt = []
    _key_space_allKeys = []
    # keep track of which components have finished
    instructionComponents = [instruction_text, key_space]
    for thisComponent in instructionComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "instruction" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *instruction_text* updates
        
        # if instruction_text is starting this frame...
        if instruction_text.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            instruction_text.frameNStart = frameN  # exact frame index
            instruction_text.tStart = t  # local t and not account for scr refresh
            instruction_text.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(instruction_text, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'instruction_text.started')
            # update status
            instruction_text.status = STARTED
            instruction_text.setAutoDraw(True)
        
        # if instruction_text is active this frame...
        if instruction_text.status == STARTED:
            # update params
            pass
        
        # *key_space* updates
        waitOnFlip = False
        
        # if key_space is starting this frame...
        if key_space.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            key_space.frameNStart = frameN  # exact frame index
            key_space.tStart = t  # local t and not account for scr refresh
            key_space.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(key_space, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'key_space.started')
            # update status
            key_space.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(key_space.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(key_space.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if key_space.status == STARTED and not waitOnFlip:
            theseKeys = key_space.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _key_space_allKeys.extend(theseKeys)
            if len(_key_space_allKeys):
                key_space.keys = _key_space_allKeys[-1].name  # just the last key pressed
                key_space.rt = _key_space_allKeys[-1].rt
                key_space.duration = _key_space_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in instructionComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "instruction" ---
    for thisComponent in instructionComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('instruction.stopped', globalClock.getTime())
    # check responses
    if key_space.keys in ['', [], None]:  # No response was made
        key_space.keys = None
    thisExp.addData('key_space.keys',key_space.keys)
    if key_space.keys != None:  # we had a response
        thisExp.addData('key_space.rt', key_space.rt)
        thisExp.addData('key_space.duration', key_space.duration)
    thisExp.nextEntry()
    # the Routine "instruction" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # --- Prepare to start Routine "instruction2" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('instruction2.started', globalClock.getTime())
    instr2_space.keys = []
    instr2_space.rt = []
    _instr2_space_allKeys = []
    # keep track of which components have finished
    instruction2Components = [text, instr2_space, response_square, response_plus, response_cross]
    for thisComponent in instruction2Components:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "instruction2" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *text* updates
        
        # if text is starting this frame...
        if text.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            text.frameNStart = frameN  # exact frame index
            text.tStart = t  # local t and not account for scr refresh
            text.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(text, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'text.started')
            # update status
            text.status = STARTED
            text.setAutoDraw(True)
        
        # if text is active this frame...
        if text.status == STARTED:
            # update params
            pass
        
        # *instr2_space* updates
        waitOnFlip = False
        
        # if instr2_space is starting this frame...
        if instr2_space.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            instr2_space.frameNStart = frameN  # exact frame index
            instr2_space.tStart = t  # local t and not account for scr refresh
            instr2_space.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(instr2_space, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'instr2_space.started')
            # update status
            instr2_space.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(instr2_space.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(instr2_space.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if instr2_space.status == STARTED and not waitOnFlip:
            theseKeys = instr2_space.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _instr2_space_allKeys.extend(theseKeys)
            if len(_instr2_space_allKeys):
                instr2_space.keys = _instr2_space_allKeys[-1].name  # just the last key pressed
                instr2_space.rt = _instr2_space_allKeys[-1].rt
                instr2_space.duration = _instr2_space_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # *response_square* updates
        
        # if response_square is starting this frame...
        if response_square.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            response_square.frameNStart = frameN  # exact frame index
            response_square.tStart = t  # local t and not account for scr refresh
            response_square.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(response_square, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'response_square.started')
            # update status
            response_square.status = STARTED
            response_square.setAutoDraw(True)
        
        # if response_square is active this frame...
        if response_square.status == STARTED:
            # update params
            pass
        
        # *response_plus* updates
        
        # if response_plus is starting this frame...
        if response_plus.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            response_plus.frameNStart = frameN  # exact frame index
            response_plus.tStart = t  # local t and not account for scr refresh
            response_plus.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(response_plus, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'response_plus.started')
            # update status
            response_plus.status = STARTED
            response_plus.setAutoDraw(True)
        
        # if response_plus is active this frame...
        if response_plus.status == STARTED:
            # update params
            pass
        
        # *response_cross* updates
        
        # if response_cross is starting this frame...
        if response_cross.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            response_cross.frameNStart = frameN  # exact frame index
            response_cross.tStart = t  # local t and not account for scr refresh
            response_cross.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(response_cross, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'response_cross.started')
            # update status
            response_cross.status = STARTED
            response_cross.setAutoDraw(True)
        
        # if response_cross is active this frame...
        if response_cross.status == STARTED:
            # update params
            pass
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in instruction2Components:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "instruction2" ---
    for thisComponent in instruction2Components:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('instruction2.stopped', globalClock.getTime())
    # check responses
    if instr2_space.keys in ['', [], None]:  # No response was made
        instr2_space.keys = None
    thisExp.addData('instr2_space.keys',instr2_space.keys)
    if instr2_space.keys != None:  # we had a response
        thisExp.addData('instr2_space.rt', instr2_space.rt)
        thisExp.addData('instr2_space.duration', instr2_space.duration)
    thisExp.nextEntry()
    # the Routine "instruction2" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # set up handler to look after randomisation of conditions etc
    Practice_loop = data.TrialHandler(nReps=1.0, method='random', 
        extraInfo=expInfo, originPath=-1,
        trialList=data.importConditions('shapes_targets.csv'),
        seed=None, name='Practice_loop')
    thisExp.addLoop(Practice_loop)  # add the loop to the experiment
    thisPractice_loop = Practice_loop.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisPractice_loop.rgb)
    if thisPractice_loop != None:
        for paramName in thisPractice_loop:
            globals()[paramName] = thisPractice_loop[paramName]
    
    for thisPractice_loop in Practice_loop:
        currentLoop = Practice_loop
        thisExp.timestampOnFlip(win, 'thisRow.t')
        # pause experiment here if requested
        if thisExp.status == PAUSED:
            pauseExperiment(
                thisExp=thisExp, 
                inputs=inputs, 
                win=win, 
                timers=[routineTimer], 
                playbackComponents=[]
        )
        # abbreviate parameter names if possible (e.g. rgb = thisPractice_loop.rgb)
        if thisPractice_loop != None:
            for paramName in thisPractice_loop:
                globals()[paramName] = thisPractice_loop[paramName]
        
        # --- Prepare to start Routine "trial" ---
        continueRoutine = True
        # update component parameters for each repeat
        thisExp.addData('trial.started', globalClock.getTime())
        # Run 'Begin Routine' code from jitterOnsetPosition
        #list of possible onsets for target
        list_onsets = [1, 1.2, 1.4, 1.6, 1.8]
        
        # randomize these onsets
        shuffle(list_onsets)
        
        #pick the first value from the list 
        onset_trial = list_onsets[0]
        
        #list of possible positions for target
        list_positions = [-0.375, -0.125, 0.125, 0.375]
        
        # randomize these onsets
        shuffle(list_positions)
        
        #pick the first value from the list 
        position_trial = list_positions[0]
        
        #TargetImage is the variable in the Image field of the target_image component
        
        #path of where to find the target image
        if TargetImage == '../../stimuli/shapes/target_square.jpg': #path of where to find the target image
            
            #setting the key press that will be the correct answer
            corrAns = 'v' 
            
        #path of where to find the target image
        elif TargetImage == '../../stimuli/shapes/target_cross.jpg':
        
            #setting the key press that will be the correct answer
            corrAns = 'c'
        
        #path of where to find the target image
        elif TargetImage == '../../stimuli/shapes/target_plus.jpg':
        
            #setting the key press that will be the correct answer
            corrAns = 'b'
        target_image.setPos((position_trial, 0))
        target_image.setImage(TargetImage)
        keyboard_response.keys = []
        keyboard_response.rt = []
        _keyboard_response_allKeys = []
        # keep track of which components have finished
        trialComponents = [image_cross, image_plus, image_square, left_far, right_mid, right_far, left_mid, target_image, keyboard_response]
        for thisComponent in trialComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        frameN = -1
        
        # --- Run Routine "trial" ---
        routineForceEnded = not continueRoutine
        while continueRoutine:
            # get current time
            t = routineTimer.getTime()
            tThisFlip = win.getFutureFlipTime(clock=routineTimer)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *image_cross* updates
            
            # if image_cross is starting this frame...
            if image_cross.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                image_cross.frameNStart = frameN  # exact frame index
                image_cross.tStart = t  # local t and not account for scr refresh
                image_cross.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(image_cross, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'image_cross.started')
                # update status
                image_cross.status = STARTED
                image_cross.setAutoDraw(True)
            
            # if image_cross is active this frame...
            if image_cross.status == STARTED:
                # update params
                pass
            
            # *image_plus* updates
            
            # if image_plus is starting this frame...
            if image_plus.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                image_plus.frameNStart = frameN  # exact frame index
                image_plus.tStart = t  # local t and not account for scr refresh
                image_plus.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(image_plus, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'image_plus.started')
                # update status
                image_plus.status = STARTED
                image_plus.setAutoDraw(True)
            
            # if image_plus is active this frame...
            if image_plus.status == STARTED:
                # update params
                pass
            
            # *image_square* updates
            
            # if image_square is starting this frame...
            if image_square.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                image_square.frameNStart = frameN  # exact frame index
                image_square.tStart = t  # local t and not account for scr refresh
                image_square.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(image_square, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'image_square.started')
                # update status
                image_square.status = STARTED
                image_square.setAutoDraw(True)
            
            # if image_square is active this frame...
            if image_square.status == STARTED:
                # update params
                pass
            
            # *left_far* updates
            
            # if left_far is starting this frame...
            if left_far.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                left_far.frameNStart = frameN  # exact frame index
                left_far.tStart = t  # local t and not account for scr refresh
                left_far.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(left_far, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'left_far.started')
                # update status
                left_far.status = STARTED
                left_far.setAutoDraw(True)
            
            # if left_far is active this frame...
            if left_far.status == STARTED:
                # update params
                pass
            
            # *right_mid* updates
            
            # if right_mid is starting this frame...
            if right_mid.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                right_mid.frameNStart = frameN  # exact frame index
                right_mid.tStart = t  # local t and not account for scr refresh
                right_mid.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(right_mid, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'right_mid.started')
                # update status
                right_mid.status = STARTED
                right_mid.setAutoDraw(True)
            
            # if right_mid is active this frame...
            if right_mid.status == STARTED:
                # update params
                pass
            
            # *right_far* updates
            
            # if right_far is starting this frame...
            if right_far.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                right_far.frameNStart = frameN  # exact frame index
                right_far.tStart = t  # local t and not account for scr refresh
                right_far.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(right_far, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'right_far.started')
                # update status
                right_far.status = STARTED
                right_far.setAutoDraw(True)
            
            # if right_far is active this frame...
            if right_far.status == STARTED:
                # update params
                pass
            
            # *left_mid* updates
            
            # if left_mid is starting this frame...
            if left_mid.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                left_mid.frameNStart = frameN  # exact frame index
                left_mid.tStart = t  # local t and not account for scr refresh
                left_mid.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(left_mid, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'left_mid.started')
                # update status
                left_mid.status = STARTED
                left_mid.setAutoDraw(True)
            
            # if left_mid is active this frame...
            if left_mid.status == STARTED:
                # update params
                pass
            
            # *target_image* updates
            
            # if target_image is starting this frame...
            if target_image.status == NOT_STARTED and tThisFlip >= onset_trial-frameTolerance:
                # keep track of start time/frame for later
                target_image.frameNStart = frameN  # exact frame index
                target_image.tStart = t  # local t and not account for scr refresh
                target_image.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(target_image, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'target_image.started')
                # update status
                target_image.status = STARTED
                target_image.setAutoDraw(True)
            
            # if target_image is active this frame...
            if target_image.status == STARTED:
                # update params
                pass
            
            # if target_image is stopping this frame...
            if target_image.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > target_image.tStartRefresh + 0.2-frameTolerance:
                    # keep track of stop time/frame for later
                    target_image.tStop = t  # not accounting for scr refresh
                    target_image.frameNStop = frameN  # exact frame index
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'target_image.stopped')
                    # update status
                    target_image.status = FINISHED
                    target_image.setAutoDraw(False)
            
            # *keyboard_response* updates
            
            # if keyboard_response is starting this frame...
            if keyboard_response.status == NOT_STARTED and t >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                keyboard_response.frameNStart = frameN  # exact frame index
                keyboard_response.tStart = t  # local t and not account for scr refresh
                keyboard_response.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(keyboard_response, 'tStartRefresh')  # time at next scr refresh
                # update status
                keyboard_response.status = STARTED
                # keyboard checking is just starting
                keyboard_response.clock.reset()  # now t=0
                keyboard_response.clearEvents(eventType='keyboard')
            if keyboard_response.status == STARTED:
                theseKeys = keyboard_response.getKeys(keyList=['b','c','v'], ignoreKeys=["escape"], waitRelease=False)
                _keyboard_response_allKeys.extend(theseKeys)
                if len(_keyboard_response_allKeys):
                    keyboard_response.keys = _keyboard_response_allKeys[-1].name  # just the last key pressed
                    keyboard_response.rt = _keyboard_response_allKeys[-1].rt
                    keyboard_response.duration = _keyboard_response_allKeys[-1].duration
                    # was this correct?
                    if (keyboard_response.keys == str(corrAns)) or (keyboard_response.keys == corrAns):
                        keyboard_response.corr = 1
                    else:
                        keyboard_response.corr = 0
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if defaultKeyboard.getKeys(keyList=["escape"]):
                thisExp.status = FINISHED
            if thisExp.status == FINISHED or endExpNow:
                endExperiment(thisExp, inputs=inputs, win=win)
                return
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                routineForceEnded = True
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in trialComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # --- Ending Routine "trial" ---
        for thisComponent in trialComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        thisExp.addData('trial.stopped', globalClock.getTime())
        # check responses
        if keyboard_response.keys in ['', [], None]:  # No response was made
            keyboard_response.keys = None
            # was no response the correct answer?!
            if str(corrAns).lower() == 'none':
               keyboard_response.corr = 1;  # correct non-response
            else:
               keyboard_response.corr = 0;  # failed to respond (incorrectly)
        # store data for Practice_loop (TrialHandler)
        Practice_loop.addData('keyboard_response.keys',keyboard_response.keys)
        Practice_loop.addData('keyboard_response.corr', keyboard_response.corr)
        if keyboard_response.keys != None:  # we had a response
            Practice_loop.addData('keyboard_response.rt', keyboard_response.rt)
            Practice_loop.addData('keyboard_response.duration', keyboard_response.duration)
        # Run 'End Routine' code from compRTandAcc
        #this code is to record the reaction times and accuracy of the trial
        
        thisRoutineDuration = t # how long did this trial last
        
        # keyboard_response.rt is the time with which a key was pressed
        # thisRecRT - how long it took for participants to respond after onset of target_image
        # thisAcc - whether or not the response was correct
        
        # compute RT based on onset of target
        thisRecRT = keyboard_response.rt - onset_trial 
        
        # check of the response was correct
        if keyboard_response.corr == 1: 
        
            # if it was correct, assign 'correct' value
            thisAcc = 'correct' 
        
        # if not, assign 'incorrect' value
        else:
            thisAcc = 'incorrect'
        
        # record the actual response times of each trial 
        thisExp.addData('trialRespTimes', thisRecRT) 
        
        # the Routine "trial" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        
        # --- Prepare to start Routine "practice_feedback" ---
        continueRoutine = True
        # update component parameters for each repeat
        thisExp.addData('practice_feedback.started', globalClock.getTime())
        feedback_text.setText("The last recorded RT was: "  + str(round(thisRecRT, 3)) + " \nThe response was: "  + thisAcc + " \n\nPress the space bar to continue."  
        )
        keyboard_response_feedback_.keys = []
        keyboard_response_feedback_.rt = []
        _keyboard_response_feedback__allKeys = []
        # keep track of which components have finished
        practice_feedbackComponents = [feedback_text, keyboard_response_feedback_]
        for thisComponent in practice_feedbackComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        frameN = -1
        
        # --- Run Routine "practice_feedback" ---
        routineForceEnded = not continueRoutine
        while continueRoutine:
            # get current time
            t = routineTimer.getTime()
            tThisFlip = win.getFutureFlipTime(clock=routineTimer)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *feedback_text* updates
            
            # if feedback_text is starting this frame...
            if feedback_text.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                feedback_text.frameNStart = frameN  # exact frame index
                feedback_text.tStart = t  # local t and not account for scr refresh
                feedback_text.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(feedback_text, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'feedback_text.started')
                # update status
                feedback_text.status = STARTED
                feedback_text.setAutoDraw(True)
            
            # if feedback_text is active this frame...
            if feedback_text.status == STARTED:
                # update params
                pass
            
            # *keyboard_response_feedback_* updates
            waitOnFlip = False
            
            # if keyboard_response_feedback_ is starting this frame...
            if keyboard_response_feedback_.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                keyboard_response_feedback_.frameNStart = frameN  # exact frame index
                keyboard_response_feedback_.tStart = t  # local t and not account for scr refresh
                keyboard_response_feedback_.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(keyboard_response_feedback_, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'keyboard_response_feedback_.started')
                # update status
                keyboard_response_feedback_.status = STARTED
                # keyboard checking is just starting
                waitOnFlip = True
                win.callOnFlip(keyboard_response_feedback_.clock.reset)  # t=0 on next screen flip
                win.callOnFlip(keyboard_response_feedback_.clearEvents, eventType='keyboard')  # clear events on next screen flip
            if keyboard_response_feedback_.status == STARTED and not waitOnFlip:
                theseKeys = keyboard_response_feedback_.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
                _keyboard_response_feedback__allKeys.extend(theseKeys)
                if len(_keyboard_response_feedback__allKeys):
                    keyboard_response_feedback_.keys = _keyboard_response_feedback__allKeys[-1].name  # just the last key pressed
                    keyboard_response_feedback_.rt = _keyboard_response_feedback__allKeys[-1].rt
                    keyboard_response_feedback_.duration = _keyboard_response_feedback__allKeys[-1].duration
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if defaultKeyboard.getKeys(keyList=["escape"]):
                thisExp.status = FINISHED
            if thisExp.status == FINISHED or endExpNow:
                endExperiment(thisExp, inputs=inputs, win=win)
                return
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                routineForceEnded = True
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in practice_feedbackComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # --- Ending Routine "practice_feedback" ---
        for thisComponent in practice_feedbackComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        thisExp.addData('practice_feedback.stopped', globalClock.getTime())
        # check responses
        if keyboard_response_feedback_.keys in ['', [], None]:  # No response was made
            keyboard_response_feedback_.keys = None
        Practice_loop.addData('keyboard_response_feedback_.keys',keyboard_response_feedback_.keys)
        if keyboard_response_feedback_.keys != None:  # we had a response
            Practice_loop.addData('keyboard_response_feedback_.rt', keyboard_response_feedback_.rt)
            Practice_loop.addData('keyboard_response_feedback_.duration', keyboard_response_feedback_.duration)
        # the Routine "practice_feedback" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        thisExp.nextEntry()
        
        if thisSession is not None:
            # if running in a Session with a Liaison client, send data up to now
            thisSession.sendExperimentData()
    # completed 1.0 repeats of 'Practice_loop'
    
    # get names of stimulus parameters
    if Practice_loop.trialList in ([], [None], None):
        params = []
    else:
        params = Practice_loop.trialList[0].keys()
    # save data for this loop
    Practice_loop.saveAsText(filename + 'Practice_loop.csv', delim=',',
        stimOut=params,
        dataOut=['n','all_mean','all_std', 'all_raw'])
    
    # --- Prepare to start Routine "instruction3" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('instruction3.started', globalClock.getTime())
    key_space_2.keys = []
    key_space_2.rt = []
    _key_space_2_allKeys = []
    # keep track of which components have finished
    instruction3Components = [end_practice_text, key_space_2]
    for thisComponent in instruction3Components:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "instruction3" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *end_practice_text* updates
        
        # if end_practice_text is starting this frame...
        if end_practice_text.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            end_practice_text.frameNStart = frameN  # exact frame index
            end_practice_text.tStart = t  # local t and not account for scr refresh
            end_practice_text.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(end_practice_text, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'end_practice_text.started')
            # update status
            end_practice_text.status = STARTED
            end_practice_text.setAutoDraw(True)
        
        # if end_practice_text is active this frame...
        if end_practice_text.status == STARTED:
            # update params
            pass
        
        # *key_space_2* updates
        waitOnFlip = False
        
        # if key_space_2 is starting this frame...
        if key_space_2.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            key_space_2.frameNStart = frameN  # exact frame index
            key_space_2.tStart = t  # local t and not account for scr refresh
            key_space_2.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(key_space_2, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'key_space_2.started')
            # update status
            key_space_2.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(key_space_2.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(key_space_2.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if key_space_2.status == STARTED and not waitOnFlip:
            theseKeys = key_space_2.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _key_space_2_allKeys.extend(theseKeys)
            if len(_key_space_2_allKeys):
                key_space_2.keys = _key_space_2_allKeys[-1].name  # just the last key pressed
                key_space_2.rt = _key_space_2_allKeys[-1].rt
                key_space_2.duration = _key_space_2_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in instruction3Components:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "instruction3" ---
    for thisComponent in instruction3Components:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('instruction3.stopped', globalClock.getTime())
    # check responses
    if key_space_2.keys in ['', [], None]:  # No response was made
        key_space_2.keys = None
    thisExp.addData('key_space_2.keys',key_space_2.keys)
    if key_space_2.keys != None:  # we had a response
        thisExp.addData('key_space_2.rt', key_space_2.rt)
        thisExp.addData('key_space_2.duration', key_space_2.duration)
    thisExp.nextEntry()
    # the Routine "instruction3" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # set up handler to look after randomisation of conditions etc
    exp_trials = data.TrialHandler(nReps=2.0, method='random', 
        extraInfo=expInfo, originPath=-1,
        trialList=data.importConditions('shapes_targets.csv'),
        seed=None, name='exp_trials')
    thisExp.addLoop(exp_trials)  # add the loop to the experiment
    thisExp_trial = exp_trials.trialList[0]  # so we can initialise stimuli with some values
    # abbreviate parameter names if possible (e.g. rgb = thisExp_trial.rgb)
    if thisExp_trial != None:
        for paramName in thisExp_trial:
            globals()[paramName] = thisExp_trial[paramName]
    
    for thisExp_trial in exp_trials:
        currentLoop = exp_trials
        thisExp.timestampOnFlip(win, 'thisRow.t')
        # pause experiment here if requested
        if thisExp.status == PAUSED:
            pauseExperiment(
                thisExp=thisExp, 
                inputs=inputs, 
                win=win, 
                timers=[routineTimer], 
                playbackComponents=[]
        )
        # abbreviate parameter names if possible (e.g. rgb = thisExp_trial.rgb)
        if thisExp_trial != None:
            for paramName in thisExp_trial:
                globals()[paramName] = thisExp_trial[paramName]
        
        # --- Prepare to start Routine "trial" ---
        continueRoutine = True
        # update component parameters for each repeat
        thisExp.addData('trial.started', globalClock.getTime())
        # Run 'Begin Routine' code from jitterOnsetPosition
        #list of possible onsets for target
        list_onsets = [1, 1.2, 1.4, 1.6, 1.8]
        
        # randomize these onsets
        shuffle(list_onsets)
        
        #pick the first value from the list 
        onset_trial = list_onsets[0]
        
        #list of possible positions for target
        list_positions = [-0.375, -0.125, 0.125, 0.375]
        
        # randomize these onsets
        shuffle(list_positions)
        
        #pick the first value from the list 
        position_trial = list_positions[0]
        
        #TargetImage is the variable in the Image field of the target_image component
        
        #path of where to find the target image
        if TargetImage == '../../stimuli/shapes/target_square.jpg': #path of where to find the target image
            
            #setting the key press that will be the correct answer
            corrAns = 'v' 
            
        #path of where to find the target image
        elif TargetImage == '../../stimuli/shapes/target_cross.jpg':
        
            #setting the key press that will be the correct answer
            corrAns = 'c'
        
        #path of where to find the target image
        elif TargetImage == '../../stimuli/shapes/target_plus.jpg':
        
            #setting the key press that will be the correct answer
            corrAns = 'b'
        target_image.setPos((position_trial, 0))
        target_image.setImage(TargetImage)
        keyboard_response.keys = []
        keyboard_response.rt = []
        _keyboard_response_allKeys = []
        # keep track of which components have finished
        trialComponents = [image_cross, image_plus, image_square, left_far, right_mid, right_far, left_mid, target_image, keyboard_response]
        for thisComponent in trialComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        frameN = -1
        
        # --- Run Routine "trial" ---
        routineForceEnded = not continueRoutine
        while continueRoutine:
            # get current time
            t = routineTimer.getTime()
            tThisFlip = win.getFutureFlipTime(clock=routineTimer)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *image_cross* updates
            
            # if image_cross is starting this frame...
            if image_cross.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                image_cross.frameNStart = frameN  # exact frame index
                image_cross.tStart = t  # local t and not account for scr refresh
                image_cross.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(image_cross, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'image_cross.started')
                # update status
                image_cross.status = STARTED
                image_cross.setAutoDraw(True)
            
            # if image_cross is active this frame...
            if image_cross.status == STARTED:
                # update params
                pass
            
            # *image_plus* updates
            
            # if image_plus is starting this frame...
            if image_plus.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                image_plus.frameNStart = frameN  # exact frame index
                image_plus.tStart = t  # local t and not account for scr refresh
                image_plus.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(image_plus, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'image_plus.started')
                # update status
                image_plus.status = STARTED
                image_plus.setAutoDraw(True)
            
            # if image_plus is active this frame...
            if image_plus.status == STARTED:
                # update params
                pass
            
            # *image_square* updates
            
            # if image_square is starting this frame...
            if image_square.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                image_square.frameNStart = frameN  # exact frame index
                image_square.tStart = t  # local t and not account for scr refresh
                image_square.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(image_square, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'image_square.started')
                # update status
                image_square.status = STARTED
                image_square.setAutoDraw(True)
            
            # if image_square is active this frame...
            if image_square.status == STARTED:
                # update params
                pass
            
            # *left_far* updates
            
            # if left_far is starting this frame...
            if left_far.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                left_far.frameNStart = frameN  # exact frame index
                left_far.tStart = t  # local t and not account for scr refresh
                left_far.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(left_far, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'left_far.started')
                # update status
                left_far.status = STARTED
                left_far.setAutoDraw(True)
            
            # if left_far is active this frame...
            if left_far.status == STARTED:
                # update params
                pass
            
            # *right_mid* updates
            
            # if right_mid is starting this frame...
            if right_mid.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                right_mid.frameNStart = frameN  # exact frame index
                right_mid.tStart = t  # local t and not account for scr refresh
                right_mid.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(right_mid, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'right_mid.started')
                # update status
                right_mid.status = STARTED
                right_mid.setAutoDraw(True)
            
            # if right_mid is active this frame...
            if right_mid.status == STARTED:
                # update params
                pass
            
            # *right_far* updates
            
            # if right_far is starting this frame...
            if right_far.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                right_far.frameNStart = frameN  # exact frame index
                right_far.tStart = t  # local t and not account for scr refresh
                right_far.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(right_far, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'right_far.started')
                # update status
                right_far.status = STARTED
                right_far.setAutoDraw(True)
            
            # if right_far is active this frame...
            if right_far.status == STARTED:
                # update params
                pass
            
            # *left_mid* updates
            
            # if left_mid is starting this frame...
            if left_mid.status == NOT_STARTED and tThisFlip >= 0.5-frameTolerance:
                # keep track of start time/frame for later
                left_mid.frameNStart = frameN  # exact frame index
                left_mid.tStart = t  # local t and not account for scr refresh
                left_mid.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(left_mid, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'left_mid.started')
                # update status
                left_mid.status = STARTED
                left_mid.setAutoDraw(True)
            
            # if left_mid is active this frame...
            if left_mid.status == STARTED:
                # update params
                pass
            
            # *target_image* updates
            
            # if target_image is starting this frame...
            if target_image.status == NOT_STARTED and tThisFlip >= onset_trial-frameTolerance:
                # keep track of start time/frame for later
                target_image.frameNStart = frameN  # exact frame index
                target_image.tStart = t  # local t and not account for scr refresh
                target_image.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(target_image, 'tStartRefresh')  # time at next scr refresh
                # add timestamp to datafile
                thisExp.timestampOnFlip(win, 'target_image.started')
                # update status
                target_image.status = STARTED
                target_image.setAutoDraw(True)
            
            # if target_image is active this frame...
            if target_image.status == STARTED:
                # update params
                pass
            
            # if target_image is stopping this frame...
            if target_image.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > target_image.tStartRefresh + 0.2-frameTolerance:
                    # keep track of stop time/frame for later
                    target_image.tStop = t  # not accounting for scr refresh
                    target_image.frameNStop = frameN  # exact frame index
                    # add timestamp to datafile
                    thisExp.timestampOnFlip(win, 'target_image.stopped')
                    # update status
                    target_image.status = FINISHED
                    target_image.setAutoDraw(False)
            
            # *keyboard_response* updates
            
            # if keyboard_response is starting this frame...
            if keyboard_response.status == NOT_STARTED and t >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                keyboard_response.frameNStart = frameN  # exact frame index
                keyboard_response.tStart = t  # local t and not account for scr refresh
                keyboard_response.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(keyboard_response, 'tStartRefresh')  # time at next scr refresh
                # update status
                keyboard_response.status = STARTED
                # keyboard checking is just starting
                keyboard_response.clock.reset()  # now t=0
                keyboard_response.clearEvents(eventType='keyboard')
            if keyboard_response.status == STARTED:
                theseKeys = keyboard_response.getKeys(keyList=['b','c','v'], ignoreKeys=["escape"], waitRelease=False)
                _keyboard_response_allKeys.extend(theseKeys)
                if len(_keyboard_response_allKeys):
                    keyboard_response.keys = _keyboard_response_allKeys[-1].name  # just the last key pressed
                    keyboard_response.rt = _keyboard_response_allKeys[-1].rt
                    keyboard_response.duration = _keyboard_response_allKeys[-1].duration
                    # was this correct?
                    if (keyboard_response.keys == str(corrAns)) or (keyboard_response.keys == corrAns):
                        keyboard_response.corr = 1
                    else:
                        keyboard_response.corr = 0
                    # a response ends the routine
                    continueRoutine = False
            
            # check for quit (typically the Esc key)
            if defaultKeyboard.getKeys(keyList=["escape"]):
                thisExp.status = FINISHED
            if thisExp.status == FINISHED or endExpNow:
                endExperiment(thisExp, inputs=inputs, win=win)
                return
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                routineForceEnded = True
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in trialComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # --- Ending Routine "trial" ---
        for thisComponent in trialComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        thisExp.addData('trial.stopped', globalClock.getTime())
        # check responses
        if keyboard_response.keys in ['', [], None]:  # No response was made
            keyboard_response.keys = None
            # was no response the correct answer?!
            if str(corrAns).lower() == 'none':
               keyboard_response.corr = 1;  # correct non-response
            else:
               keyboard_response.corr = 0;  # failed to respond (incorrectly)
        # store data for exp_trials (TrialHandler)
        exp_trials.addData('keyboard_response.keys',keyboard_response.keys)
        exp_trials.addData('keyboard_response.corr', keyboard_response.corr)
        if keyboard_response.keys != None:  # we had a response
            exp_trials.addData('keyboard_response.rt', keyboard_response.rt)
            exp_trials.addData('keyboard_response.duration', keyboard_response.duration)
        # Run 'End Routine' code from compRTandAcc
        #this code is to record the reaction times and accuracy of the trial
        
        thisRoutineDuration = t # how long did this trial last
        
        # keyboard_response.rt is the time with which a key was pressed
        # thisRecRT - how long it took for participants to respond after onset of target_image
        # thisAcc - whether or not the response was correct
        
        # compute RT based on onset of target
        thisRecRT = keyboard_response.rt - onset_trial 
        
        # check of the response was correct
        if keyboard_response.corr == 1: 
        
            # if it was correct, assign 'correct' value
            thisAcc = 'correct' 
        
        # if not, assign 'incorrect' value
        else:
            thisAcc = 'incorrect'
        
        # record the actual response times of each trial 
        thisExp.addData('trialRespTimes', thisRecRT) 
        
        # the Routine "trial" was not non-slip safe, so reset the non-slip timer
        routineTimer.reset()
        thisExp.nextEntry()
        
        if thisSession is not None:
            # if running in a Session with a Liaison client, send data up to now
            thisSession.sendExperimentData()
    # completed 2.0 repeats of 'exp_trials'
    
    # get names of stimulus parameters
    if exp_trials.trialList in ([], [None], None):
        params = []
    else:
        params = exp_trials.trialList[0].keys()
    # save data for this loop
    exp_trials.saveAsText(filename + 'exp_trials.csv', delim=',',
        stimOut=params,
        dataOut=['n','all_mean','all_std', 'all_raw'])
    
    # --- Prepare to start Routine "end_screen" ---
    continueRoutine = True
    # update component parameters for each repeat
    thisExp.addData('end_screen.started', globalClock.getTime())
    space_end.keys = []
    space_end.rt = []
    _space_end_allKeys = []
    # keep track of which components have finished
    end_screenComponents = [end_message, space_end]
    for thisComponent in end_screenComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    frameN = -1
    
    # --- Run Routine "end_screen" ---
    routineForceEnded = not continueRoutine
    while continueRoutine:
        # get current time
        t = routineTimer.getTime()
        tThisFlip = win.getFutureFlipTime(clock=routineTimer)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *end_message* updates
        
        # if end_message is starting this frame...
        if end_message.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            end_message.frameNStart = frameN  # exact frame index
            end_message.tStart = t  # local t and not account for scr refresh
            end_message.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(end_message, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'end_message.started')
            # update status
            end_message.status = STARTED
            end_message.setAutoDraw(True)
        
        # if end_message is active this frame...
        if end_message.status == STARTED:
            # update params
            pass
        
        # *space_end* updates
        waitOnFlip = False
        
        # if space_end is starting this frame...
        if space_end.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            space_end.frameNStart = frameN  # exact frame index
            space_end.tStart = t  # local t and not account for scr refresh
            space_end.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(space_end, 'tStartRefresh')  # time at next scr refresh
            # add timestamp to datafile
            thisExp.timestampOnFlip(win, 'space_end.started')
            # update status
            space_end.status = STARTED
            # keyboard checking is just starting
            waitOnFlip = True
            win.callOnFlip(space_end.clock.reset)  # t=0 on next screen flip
            win.callOnFlip(space_end.clearEvents, eventType='keyboard')  # clear events on next screen flip
        if space_end.status == STARTED and not waitOnFlip:
            theseKeys = space_end.getKeys(keyList=['space'], ignoreKeys=["escape"], waitRelease=False)
            _space_end_allKeys.extend(theseKeys)
            if len(_space_end_allKeys):
                space_end.keys = _space_end_allKeys[-1].name  # just the last key pressed
                space_end.rt = _space_end_allKeys[-1].rt
                space_end.duration = _space_end_allKeys[-1].duration
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if defaultKeyboard.getKeys(keyList=["escape"]):
            thisExp.status = FINISHED
        if thisExp.status == FINISHED or endExpNow:
            endExperiment(thisExp, inputs=inputs, win=win)
            return
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            routineForceEnded = True
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in end_screenComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # --- Ending Routine "end_screen" ---
    for thisComponent in end_screenComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    thisExp.addData('end_screen.stopped', globalClock.getTime())
    # check responses
    if space_end.keys in ['', [], None]:  # No response was made
        space_end.keys = None
    thisExp.addData('space_end.keys',space_end.keys)
    if space_end.keys != None:  # we had a response
        thisExp.addData('space_end.rt', space_end.rt)
        thisExp.addData('space_end.duration', space_end.duration)
    thisExp.nextEntry()
    # the Routine "end_screen" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # mark experiment as finished
    endExperiment(thisExp, win=win, inputs=inputs)


def saveData(thisExp):
    """
    Save data from this experiment
    
    Parameters
    ==========
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    """
    filename = thisExp.dataFileName
    # these shouldn't be strictly necessary (should auto-save)
    thisExp.saveAsPickle(filename)


def endExperiment(thisExp, inputs=None, win=None):
    """
    End this experiment, performing final shut down operations.
    
    This function does NOT close the window or end the Python process - use `quit` for this.
    
    Parameters
    ==========
    thisExp : psychopy.data.ExperimentHandler
        Handler object for this experiment, contains the data to save and information about 
        where to save it to.
    inputs : dict
        Dictionary of input devices by name.
    win : psychopy.visual.Window
        Window for this experiment.
    """
    if win is not None:
        # remove autodraw from all current components
        win.clearAutoDraw()
        # Flip one final time so any remaining win.callOnFlip() 
        # and win.timeOnFlip() tasks get executed
        win.flip()
    # mark experiment handler as finished
    thisExp.status = FINISHED
    # shut down eyetracker, if there is one
    if inputs is not None:
        if 'eyetracker' in inputs and inputs['eyetracker'] is not None:
            inputs['eyetracker'].setConnectionState(False)
    logging.flush()


def quit(thisExp, win=None, inputs=None, thisSession=None):
    """
    Fully quit, closing the window and ending the Python process.
    
    Parameters
    ==========
    win : psychopy.visual.Window
        Window to close.
    inputs : dict
        Dictionary of input devices by name.
    thisSession : psychopy.session.Session or None
        Handle of the Session object this experiment is being run from, if any.
    """
    thisExp.abort()  # or data files will save again on exit
    # make sure everything is closed down
    if win is not None:
        # Flip one final time so any remaining win.callOnFlip() 
        # and win.timeOnFlip() tasks get executed before quitting
        win.flip()
        win.close()
    if inputs is not None:
        if 'eyetracker' in inputs and inputs['eyetracker'] is not None:
            inputs['eyetracker'].setConnectionState(False)
    logging.flush()
    if thisSession is not None:
        thisSession.stop()
    # terminate Python process
    core.quit()


# if running this experiment as a script...
if __name__ == '__main__':
    # call all functions in order
    expInfo = showExpInfoDlg(expInfo=expInfo)
    thisExp = setupData(expInfo=expInfo)
    logFile = setupLogging(filename=thisExp.dataFileName)
    win = setupWindow(expInfo=expInfo)
    inputs = setupInputs(expInfo=expInfo, thisExp=thisExp, win=win)
    run(
        expInfo=expInfo, 
        thisExp=thisExp, 
        win=win, 
        inputs=inputs
    )
    saveData(thisExp=thisExp)
    quit(thisExp=thisExp, win=win, inputs=inputs)
